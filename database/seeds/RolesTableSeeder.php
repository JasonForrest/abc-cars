<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'manager',
            'label' => 'Site Manager'],
            
            ['name' => 'staff_member',
            'label' => 'ABC Cars Employee'],

            ['name' => 'member',
            'label' => 'Website member']            

        ]);
    }
}
