<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            ['firstName' => 'Jane',
            'lastName' => 'Doe',
            'email' => 'admin@abccars.com',
            'password' => bcrypt('password')],

            ['firstName' => 'John',
            'lastName' => 'Doe',
            'email' => 'staff@abccars.com',
            'password' => bcrypt('password')]
        ]);

        factory(App\User::class, 100)->create();

        factory(App\Supplier::class, 20)->create();
    }
}
