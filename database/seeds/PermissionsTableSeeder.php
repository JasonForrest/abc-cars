<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['name' => 'edit_user',
            'label' => 'Edit User'],

            ['name' => 'add_vehicle',
            'label' => 'Add Vehicler'],

            ['name' => 'add_user',
            'label' => 'Add User'],
            
            ['name' => 'edit_vehicle',
            'label' => 'Edit Vehicle'],
            
            ['name' => 'view_content',
            'label' => 'View Website Content'],
            
            ['name' => 'make_enquiries',
            'label' => 'Enquire about vehicles'],

            ['name' => 'access_dashboard',
            'label' => 'Access Admin Dashboard'],

            ['name' => 'add_staff_members',
            'label' => 'Add New Staff Members'],

            ['name' => 'administer_site',
            'label' => 'Administer Site']
           
        ]);
    }
}
