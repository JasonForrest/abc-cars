<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
	$faker = Faker\Factory::create("en_AU");
    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt('password'),
        'phone' => $faker->phoneNumber,
        'streetNo' => $faker->buildingNumber,
        'streetName' => $faker->streetName,
        'suburb' => $faker->city,
        'state' => $faker->state,
        'postCode' => $faker->postcode,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Supplier::class, function (Faker\Generator $faker) {
	$faker = Faker\Factory::create("en_AU");
    return [        
        'name' => $faker->firstName. ' ' .$faker->lastName,
        'abn' => $faker->isbn10,
        'phone' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'streetNo' => $faker->buildingNumber,
        'streetName' => $faker->streetName,
        'suburb' => $faker->city,
        'state' => $faker->state,
        'postCode' => $faker->postcode,
        'notes' => $faker->text($maxNbChars = 200)
    ];
});
