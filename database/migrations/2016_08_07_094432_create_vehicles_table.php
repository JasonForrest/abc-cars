<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('modelID')->unsigned();
            $table->integer('supplierID')->unsigned();
            $table->string('odometer', 24);
            $table->string('transmission', 24);
            $table->string('colour', 24);
            $table->string('vehicleCondition', 300);
            $table->decimal('price', 10, 0);
            $table->boolean('isSold');
            $table->string('kmPltrFuel', 24);
            $table->longText('serviceHistory');
            $table->longText('description');
            $table->string('img', 100);
            $table->boolean('isFeatured');
            $table->boolean('isDeleted');
            $table->timestamps();

            $table->foreign('modelID')->references('id')->on('vehicle_models');
            $table->foreign('supplierID')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
