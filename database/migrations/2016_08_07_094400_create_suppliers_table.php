<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->string('abn', 64);
            $table->string('phone', 20);
            $table->string('email', 255);
            $table->string('streetNo', 8);
            $table->string('streetName', 100);
            $table->string('suburb', 100);
            $table->string('state', 10);
            $table->string('postcode', 10);
            $table->longText('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers');
    }
}
