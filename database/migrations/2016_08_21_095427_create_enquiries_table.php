<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('enquiries', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('userID')->unsigned();
            $table->integer('vehicleID')->unsigned();
            $table->string('enquiry');
            $table->foreign('userID')->references('id')->on('users'); 
            $table->foreign('vehicleID')->references('id')->on('vehicles');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enquiries');
    }
}