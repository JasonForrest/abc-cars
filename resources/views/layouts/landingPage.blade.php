@include('parts.htmlHeader')
@include('parts.topnav')
 


{{-- Only include the banner section scripts if it exists in the view --}}
@hasSection('banner')
    <div class="container-fluid">
        @yield('banner')
    </div>
@endif    
@yield('title-row') 
    <div id="main-content" class="container">
        <div class="row">
            <div class="col-md-12 content">                
                
                
                <div id="content">

                    @yield('content')
                    
                </div>
            </div>
           
        </div>
    </div><!-- close #main-content -->
    
    {{-- Only include the lower content section scripts if it exists in the view --}}
@hasSection('lower-content')
    <div id="lower-content">
        <div class="container">
            <div class="row lc">
                    @yield('lower-content')
            </div>
        </div>
    </div>
@endif

   <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="{{ url('/') }}">Home</a> - 
                    <a href="{{ url('about') }}">About</a> - 
                    <a href="{{ url('contact') }}">Contact</a> - 
                    <a href="{{ url('privacy') }}">Privacy</a> - 
                    <a href="{{ url('terms') }}">Terms & Conditions</a>
                <p>Copyright &copy; {{ date('Y') }} Jason Forrest</p>
            </div>
        </div>

    {{-- include any javascript required on all or most pages --}}
    @include('parts.globalFooterScripts')
    
    {{-- Only include the footer scripts if the section exists in the view --}}
    @hasSection('footer-scripts')
       @yield('footer-scripts')
    @endif

    </body>
</html>
 
