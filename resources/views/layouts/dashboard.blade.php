@include('parts.htmlHeader')
 @include('parts.topnav')

        <div id="main-content" class="container-fluid">
            <div class="row">
                <div id="sidebar-left" class="col-md-2">
                    <ul class="list-group">
                        <li class="list-group-item"><a href="{{ url('dashboard/staff/lookup-member') }}">Edit/Delete Member</a></li>
                        <li class="list-group-item"><a href="{{ url('dashboard/staff/add-vehicle') }}">Add Vehicle</a></li>
                        <li class="list-group-item"><a href="{{ url('search') }}">Edit/Delete Vehicle</a></li>
                        <li class="list-group-item"><a href="{{ url('dashboard/staff/add-supplier') }}">Add Supplier</a></li>
                        <li class="list-group-item"><a href="{{ url('dashboard/staff/list-suppliers') }}">List Suppliers</a></li>
                    </ul>
                </div>
                <div class="col-md-10 col-md-offset-2 content">                
                    <div>

                        @yield('title')

                    </div>
                    <div>

                        @yield('content')

                    </div>
                </div>
            </div><!-- close row -->
        </div><!-- close #main-content -->
        <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="{{ url('/') }}">Home</a> - 
                    <a href="{{ url('about') }}">About</a> - 
                    <a href="{{ url('contact') }}">Contact</a> - 
                    <a href="{{ url('privacy') }}">Privacy</a> - 
                    <a href="{{ url('terms') }}">Terms & Conditions</a>
                <p>Copyright &copy; {{ date('Y') }} Jason Forrest</p>
            </div>
        </div>
        @include('parts.globalFooterScripts')
       
        @yield('footer-scripts')
    </body>
</html>
 
 
