@include('parts.htmlHeader')
@include('parts.topnav')

        <div class="container-fluid" style="background-color:#666;">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <img src="/img/abccars.png" alt="logo">
                    </div>
                    <div class="col-md-2 col-md-offset-6 social-icons pull-right" style="padding-top:1.5rem">
                        <i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i>
                        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
                        <i class="fa fa-2x fa-google-plus-square" aria-hidden="true"></i>
                        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>

        <div id="main-content" class="container">
        
        @hasSection('featured')
            <div class="row featured">
                @yield('featured')
            </div>
        @endif 

        <div class="row title">
            @yield('title')
        </div>

        
            @hasSection('sidebar')
            <div class="row content-wrapper">
                <div class="col-md-9 content">
                    @yield('content')
                </div>

                <div class="col-md-3 public-sidebar">
                                          
                        @yield('sidebar')
                    
                </div>
            </div><!-- close #main-content -->
            @else
            <div class="row content-wrapper">
                <div class="col-md-12 content">
                    @yield('content')
                </div>
            </div><!-- close #main-content -->
            @endif 
        </div>               
        
        
        <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="{{ url('/') }}">Home</a> - 
                    <a href="{{ url('about') }}">About</a> - 
                    <a href="{{ url('contact') }}">Contact</a> - 
                    <a href="{{ url('privacy') }}">Privacy</a> - 
                    <a href="{{ url('terms') }}">Terms & Conditions</a>
                <p>Copyright &copy; {{ date('Y') }} Jason Forrest</p>
            </div>
        </div>

        @include('parts.globalFooterScripts')
        @hasSection('footer-scripts')
            @yield('footer-scripts')
        @endif
    </body>
</html>
 
