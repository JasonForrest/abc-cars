@include('parts.htmlHeader')
 @include('parts.topnav')
        <div id="main-content" class="container">
            <div class="row">
                <div class="col-md-12 content">                
                    <div>

                        @yield('title')

                    </div>
                    
                    <div>

                        @yield('content')

                    </div>
                </div>
               
                
            </div>
        </div><!-- close #main-content -->
        <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="{{ url('/') }}">Home</a> - 
                    <a href="{{ url('about') }}">About</a> - 
                    <a href="{{ url('contact') }}">Contact</a> - 
                    <a href="{{ url('privacy') }}">Privacy</a> - 
                    <a href="{{ url('terms') }}">Terms & Conditions</a>
                <p>Copyright &copy; {{ date('Y') }} Jason Forrest</p>
            </div>
        </div>
        @include('parts.globalFooterScripts')
        @yield('footer-scripts')
    </body>
</html>
 
