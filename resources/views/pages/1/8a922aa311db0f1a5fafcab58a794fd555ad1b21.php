<?php echo $__env->make('parts.htmlHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('parts.topnav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div id="main-content" class="container-fluid">
            <div class="row">
                <div id="sidebar-left" class="col-md-2">
                    <ul class="list-group">
                        <li class="list-group-item"><a href="<?php echo e(url('/dashboard/staff/lookup-member')); ?>">Edit/Delete Member</a></li>
                        <li class="list-group-item"><a href="<?php echo e(url('/dashboard/staff/add-vehicle')); ?>">Add Vehicle</a></li>
                        <li class="list-group-item"><a href="<?php echo e(url('search')); ?>">Edit/Delete Vehicle</a></li>
                        <li class="list-group-item"><a href="<?php echo e(url('/dashboard/staff/add-supplier')); ?>">Add Supplier</a></li>
                        <li class="list-group-item"><a href="<?php echo e(url('/dashboard/staff/list-suppliers')); ?>">List Suppliers</a></li>
                    </ul>
                </div>
                <div class="col-md-10 col-md-offset-2 content">                
                    <div>

                        <?php echo $__env->yieldContent('title'); ?>

                    </div>
                    <div>

                        <?php echo $__env->yieldContent('content'); ?>

                    </div>
                </div>
            </div><!-- close row -->
        </div><!-- close #main-content -->
        
        <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="<?php echo e(url('/')); ?>">Home</a> - 
                    <a href="<?php echo e(url('about')); ?>">About</a> - 
                    <a href="<?php echo e(url('contact')); ?>">Contact</a> - 
                    <a href="<?php echo e(url('privacy')); ?>">Privacy</a> - 
                    <a href="<?php echo e(url('terms')); ?>">Terms & Conditions</a>
                </p>
                <p>Copyright &copy; <?php echo e(date('Y')); ?> Jason Forrest</p>
            </div>
        </div>
        <?php echo $__env->make('parts.globalFooterScripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       
        <?php echo $__env->yieldContent('footer-scripts'); ?>
    </body>
</html>
 
 
