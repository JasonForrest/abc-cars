<?php echo $__env->make('parts.htmlHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('parts.topnav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="container-fluid" style="background-color:#666;">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <img src="/abccars/public/img/abccars.png" alt="logo">
                    </div>
                    <div class="col-md-2 col-md-offset-6 social-icons pull-right" style="padding-top:1.5rem">
                        <i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i>
                        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
                        <i class="fa fa-2x fa-google-plus-square" aria-hidden="true"></i>
                        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>

        <div id="main-content" class="container">
        
        <?php if (! empty(trim($__env->yieldContent('featured')))): ?>
            <div class="row featured">
                <?php echo $__env->yieldContent('featured'); ?>
            </div>
        <?php endif; ?> 

        <div class="row title">
            <?php echo $__env->yieldContent('title'); ?>
        </div>

        
            <?php if (! empty(trim($__env->yieldContent('sidebar')))): ?>
            <div class="row content-wrapper">
                <div class="col-md-9 content">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>

                <div class="col-md-3 public-sidebar">
                                          
                        <?php echo $__env->yieldContent('sidebar'); ?>
                    
                </div>
            </div><!-- close #main-content -->
            <?php else: ?>
            <div class="row content-wrapper">
                <div class="col-md-12 content">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div><!-- close #main-content -->
            <?php endif; ?> 
        </div>               
        
        
        <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="<?php echo e(url('/')); ?>">Home</a> - 
                    <a href="<?php echo e(url('about')); ?>">About</a> - 
                    <a href="<?php echo e(url('contact')); ?>">Contact</a> - 
                    <a href="<?php echo e(url('privacy')); ?>">Privacy</a> - 
                    <a href="<?php echo e(url('terms')); ?>">Terms & Conditions</a>
                <p>Copyright &copy; <?php echo e(date('Y')); ?> Jason Forrest</p>
            </div>
        </div>

        <?php echo $__env->make('parts.globalFooterScripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if (! empty(trim($__env->yieldContent('footer-scripts')))): ?>
            <?php echo $__env->yieldContent('footer-scripts'); ?>
        <?php endif; ?>
    </body>
</html>
 
