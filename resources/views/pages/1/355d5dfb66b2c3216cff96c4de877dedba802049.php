<?php 
use App\VehicleModel;
use App\Supplier;
$vehicles = vehicleModel::all(); 
$suppliers = Supplier::all();
?>

	<form 
		method="post" 
		action="/abccars/public/addsupplier" 
		enctype="multipart/form-data"
		>
		<?php echo e(csrf_field()); ?>

			
			<div class="row">

				<div class="col-md-3">
					<div class="form-group">
						<label for="name">name
							<input class="form-control" id="name" name="name" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="abn">ABN
							<input class="form-control" id="abn" name="abn" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="phone">Phone
							<input class="form-control" id="phone" name="phone" type="text" value="">
						</label>
					</div>
				</div>

				
			</div><!-- end form row -->

			<div class="row">

				

				<div class="col-md-3">
					<div class="form-group">
						<label for="email">email
							<input class="form-control" id="email" name="email" type="email" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="streetNo">Street Number
							<input class="form-control" id="streetNo" name="streetNo" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="streetName">Street Name
							<input class="form-control" id="streetName" name="streetName" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="suburb">Suburb
							<input class="form-control" id="suburb" name="suburb" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="state">State
							<input class="form-control" id="state" name="state" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="postcode">Postcode
							<input class="form-control" id="postcode" name="postcode" type="text" value="">
						</label>
					</div>
				</div>
			</div><!-- end form row -->

			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="form-group">
							<label for="description">Notes
								<textarea class="form-control" id="notes" name="notes"></textarea>
							</label>
						</div>
					</div>
				</div>
			</div>

			
			<input class="btn btn-primary" id="submit" name="submit" type="submit">
		</form>