<?php $__env->startSection('title'); ?>
	<h1>New Cars</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('featured'); ?>
<?php echo $__env->make('parts.recentlyListed', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<p>The new cars page</p>
	<?php foreach($newCars as $car): ?>

		<div class="col-md-4 col-sm-6 car">
					<a href="car/<?php echo e($car->id); ?>"><strong><?php echo e($car->vehicleModel->year); ?> <?php echo e($car->vehicleModel->make); ?> <?php echo e($car->vehicleModel->model); ?></strong>
						<br>
						<img class="img-responsive thumbnail" height="100" src="<?php echo e($car->img); ?>">
					</a>
					
					<?php if($car->isSold): ?><h4 class="pull-left text-danger">Vehicle SOLD</h4><?php endif; ?><h4 style="text-align:right;">Price $<?php echo e($car->price); ?></h4>	
			
		</div>

	<?php endforeach; ?>
	<div class="row">
		<div class="col-md-6 col-md-offset-4">
			<?php echo e($newCars->links()); ?>

		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>