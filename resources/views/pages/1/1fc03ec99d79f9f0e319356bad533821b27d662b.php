<div class="modal fade confirmDelete" tabindex="-1" role="dialog" aria-labelledby="modal-title">
  <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
            <h4>Please confirm</h4>
          </div>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete?</p>
        </div>
        <div class="modal-footer">
            <input class="btn btn-danger pull-right" type="submit" name="submit" value="Delete vehicle">
    
          <button class="btn btn-primary pull-left"  data-dismiss="modal">No thanks</button>
          
        </div>
      </div>
  </div>
</div>