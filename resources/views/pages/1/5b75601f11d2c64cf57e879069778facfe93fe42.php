<?php $__env->startSection('title'); ?>
	<h1>Staff Dashboard</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
	<?php echo e(display_errors($errors)); ?>

	
	<div>
		
		<div class="panel panel-default">
            <div class="panel-heading"><h4>Add a supplier</h4></div>
                <div class="panel-body">
					<?php echo $__env->make('forms.addSupplierForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>

  	</div>

</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>