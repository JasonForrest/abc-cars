<?php $__env->startSection('content'); ?>
<?php if(Session::has('message')): ?>
    <div class="alert alert-info">
      <?php echo e(Session::get('message')); ?>

    </div>
<?php endif; ?>
<div class="panel panel-default contact">
	<div class="panel-heading">Contact Us</div>
		<div class="panel-body">

		
		<ul>
    <?php foreach($errors->all() as $error): ?>
        <li><?php echo e($error); ?></li>
    <?php endforeach; ?>
</ul>

<?php echo Form::open(array('route' => 'contact_store', 'class' => 'form')); ?>


<div class="form-group">
    <?php echo Form::label('Your Name'); ?>

    <?php echo Form::text('name', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your name')); ?>

</div>

<div class="form-group">
    <?php echo Form::label('Your E-mail Address'); ?>

    <?php echo Form::text('email', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your e-mail address')); ?>

</div>

<div class="form-group">
    <?php echo Form::label('Your Message'); ?>

    <?php echo Form::textarea('message', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your message')); ?>

</div>

<div class="form-group">
    <?php echo Form::submit('Contact Us!', 
      array('class'=>'btn btn-primary')); ?>

</div>
<?php echo Form::close(); ?>

</div>

</div>

<?php $__env->stopSection(); ?>

<?php /* <?php $__env->startSection('sidebar'); ?>
	<h4>Recently listed</h4>
    <ul>
	    <?php foreach($latestFeaturedVehicles as $vehicle): ?>
	    	<li><a href="car/<?php echo e($vehicle->id); ?>"><?php echo e($vehicle->vehicleModel->year); ?> <?php echo e($vehicle->vehicleModel->make); ?> <?php echo e($vehicle->vehicleModel->model); ?></a></li>

	    <?php endforeach; ?>
    <ul>
<?php $__env->stopSection(); ?> */ ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>