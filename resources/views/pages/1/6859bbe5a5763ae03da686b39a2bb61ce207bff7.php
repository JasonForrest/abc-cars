<?php 
use App\VehicleModel;
use App\Supplier;
$vehicles = vehicleModel::all(); 
$suppliers = Supplier::all();
?>

	<form 
		method="post" 
		action="/abccars/public/addcar" 
		enctype="multipart/form-data"
		>
		<?php echo e(csrf_field()); ?>

			<div class="row">
				
				<div class="col-md-3">
					<div class="form-group">
						<label for="modelID">Model
							<select class="form-control" name="modelID">
								<option value="">-- Select Model --</option>
  							<?php foreach( $vehicles as $vehicleModel): ?>
  									<option value="<?php echo e($vehicleModel->id); ?>"><?php echo e($vehicleModel->year); ?> <?php echo e($vehicleModel->make); ?> <?php echo e($vehicleModel->model); ?></option>
  							<?php endforeach; ?>
							</select>
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="supplier">Supplier
							<select class="form-control" name="supplier">
								<option value="">-- Select Supplier --</option>
  							<?php foreach( $suppliers as $supplier): ?>
  									<option value="<?php echo e($supplier->id); ?>"><?php echo e($supplier->name); ?></option>
  							<?php endforeach; ?>
							</select>
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="odometer">Mileage
							<input class="form-control" id="odometer" name="odometer" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="transmission">Transmission
							<select id="transmission" class="form-control" name="transmission">
								<option>-- Select Transmission --</option>
								<option value="Automatic">Automatic</option>
								<option value="Manual">Manual</option>  							
							</select>
						</label>
					</div>
				</div>
			</div><!-- end form row -->

			<div class="row">

				<div class="col-md-3">
					<div class="form-group">
						<label for="colour">Body Colour
							<input class="form-control" id="colour" name="colour" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="vehicleCondition">Vehicle Condition
							<input class="form-control" id="vehicleCondition" name="vehicleCondition" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="price">Price
							<input class="form-control" id="price" name="price" type="text" value="">
						</label>
					</div>
				</div>

				
			</div><!-- end form row -->

			<div class="row">

				<div class="col-md-3">
					<div class="form-group">
						<label for="isSold">Is Sold?
							<select class="form-control" id="isSold" name="isSold">
								<option value="Yes">Yes</option>
								<option value="No" selected>No</option>
							</select>
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="kmPltrFuel">Fuel Ecomomy km/L
							<input class="form-control" id="kmPltrFuel" name="kmPltrFuel" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="serviceHistory">Service History
							<input class="form-control" id="serviceHistory" name="serviceHistory" type="text" value="">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<label for="isFeatured">Is the vehicle featured?
						<select class="form-control" id="isFeatured" name="isFeatured">
							<option value="Yes">Yes</option>
							<option value="No" selected>No</option>
						</select>
					</label>
				</div>
			</div><!-- end form row -->

			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="form-group">
							<label for="description">Description
								<textarea class="form-control" id="description" name="description"></textarea>
							</label>
						</div>
					</div>
				</div>
			</div>

<input type="file" name="img">
			
			<input class="btn btn-primary" id="submit" name="submit" type="submit">
		</form>