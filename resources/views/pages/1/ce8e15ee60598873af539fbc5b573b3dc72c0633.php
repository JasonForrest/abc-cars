<?php $__env->startSection('title'); ?>
	<h1>Search Results</h1>
	<?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('edit_vehicle')): ?>
		<p>Search or browse for vehicles below then click edit to edit a vehicle</p>
		<p>Vehicles can also be deleted from the edit page</p>
	<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
	<form method="GET">
    	<div class="form-group">
            <input class="form-control" type="search" name="q" placeholder="Search again">
        </div>
    </form>
    
	<?php if($vehicles): ?>
		<h2>We found the following vehicles matching your search</h2>

	    <?php foreach($vehicles as $vehicle): ?>
	   
	    <ul class="list-group">
				<div class="col-md-4 car">
					<li class="list-group-item">
						<a href="car/<?php echo e($vehicle->id); ?>"><strong><?php echo e($vehicle->year); ?> <?php echo e($vehicle->make); ?> <?php echo e($vehicle->model); ?></strong>
							<br>
							<img class="img-responsive thumbnail" height="100" src="<?php echo e($vehicle->img); ?>">
						</a>
						<br>
						<h4 style="text-align:right;">$<?php echo e($vehicle->price); ?></h4>
					</li>
			
				<?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('edit_vehicle')): ?>
					<li class="list-group-item">
						<a class="btn btn-primary pull-left" href="car/<?php echo e($vehicle->id); ?>/edit">Edit vehicle</a>
						<br><br>
					</li>
				<?php endif; ?>	
				
				</div>
			</ul>	

	    <?php endforeach; ?>
	   
	<div class="row"><?php /* Display pagination links if more than 12 cars */ ?>
			<div class="col-md-6 col-md-offset-4">
				<?php echo e($vehicles->links()); ?>

			</div>
		</div>
	<?php else: ?>
		<h4>We don't have any vehicles matching your search. Try again!</h4>    
	<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.noSidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>