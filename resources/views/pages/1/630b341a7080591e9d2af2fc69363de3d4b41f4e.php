<?php $__env->startSection('title'); ?>
	<h1><?php echo e($car->vehicleModel->year); ?> <?php echo e($car->vehicleModel->make); ?> <?php echo e($car->vehicleModel->model); ?></h1>
	<?php if($car->isDeleted): ?><h4 class="alert alert-danger">Vehicle Deleted</h4><?php endif; ?>
	<?php if($car->isSold): ?><h4 class="alert alert-danger">Vehicle SOLD</h4><?php endif; ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
	<?php echo e(img_if_data_exists($car->img, 'img-responsive', $car->vehicleModel->year.' '.$car->vehicleModel->make.' '.$car->vehicleModel->model)); ?>

	<p><strong>Details:</strong> <?php echo e($car->description); ?></p>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
	<?php echo $__env->make('parts.SidebarShowCar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>