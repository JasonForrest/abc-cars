<?php $__env->startSection('title'); ?>
	<h1>Staff Dashboard</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<h2>Enquiries</h2>
	<div class="table-responsive">
	<table class="table">

	<thead>
		<th>User</th>
		<th>Email</th>
		<th>Enquiry</th>
		<th>Vehicle ID</th>
		<th>Make and Model</th>
		<th></th>
	</thead>
	<tbody>
	<?php foreach($enquiries as $enquiry): ?>
	<tr>
		<td><?php echo e($enquiry->firstName); ?> <?php echo e($enquiry->lastName); ?></td>
		<td><?php echo e($enquiry->email); ?></td>
		<td><?php echo e($enquiry->enquiry); ?></td>
		<td><?php echo e($enquiry->id); ?></td>
		<td><?php echo e($enquiry->year); ?> <?php echo e($enquiry->make); ?> <?php echo e($enquiry->model); ?></td></td>
		<td><button class="btn btn-primary" data-toggle="modal" data-target="#emailPlaceholder">Reply</button>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
  	</div>

  	<h2>Orders</h2>
	<div class="table-responsive">
	<table class="table">

	<thead>
		<th>User</th>
		<th>Email</th>
		<th>Vehicle ID</th>
		<th>Make and Model</th>
		<th></th>
	</thead>
	<tbody>
	<?php foreach($orders as $order): ?>
	<tr>
		<td><?php echo e($order->firstName); ?> <?php echo e($order->lastName); ?></td>
		<td><?php echo e($order->email); ?></td>
		<td><?php echo e($order->id); ?></td>
		<td><?php echo e($order->year); ?> <?php echo e($order->make); ?> <?php echo e($order->model); ?></td></td>
		<td><button class="btn btn-primary" data-toggle="modal" data-target="#invoicePlaceholder">Create Invoice</button>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
  	</div>

<?php echo $__env->make('modals.emailPlaceholder', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('modals.invoicePlaceholder', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>