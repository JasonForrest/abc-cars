<?php $__env->startSection('title'); ?>

<?php /* 
Because the user must be logged in to enquire about a vehicle, we can simply send the logged in user name and email to the controller. 

Enquiries will be stored in the database and available for staff to view and reply to in the dashboard. In reality there could be some kind of alert system in place. But that is beyond the scope of this exercise.

I'll also send the vehicle details in the background based on the id of the vehicle that was clicked.

So the only fillable field in this case will be the actual enquiry itself. 

Nice and eay for the user and easy enough for me :)

 */ ?>
	<h1>Vehicle Order</h1>


	<?php if(Session::has('flash_message')): ?>
                
            <div class="flash alert alert-warning"> 
                <?php echo e(Session::get('flash_message')); ?> 
            </div>
            
    <?php endif; ?>


	<h4>Vehicle order from <?php echo e(Auth::user()->name); ?></h4>
	<h4>Vehicle: <?php echo e($vehicle->vehicleModel->year); ?> <?php echo e($vehicle->vehicleModel->make); ?> <?php echo e($vehicle->vehicleModel->model); ?></h4>
	
	<?php echo e(img_if_data_exists($vehicle->img)); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo e(display_errors($errors)); ?>

	<?php echo $__env->make('forms.vehicleOrderForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<br>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>