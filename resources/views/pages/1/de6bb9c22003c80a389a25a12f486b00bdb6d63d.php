<?php $__env->startSection('title'); ?>
  <h1>Staff Dashboard</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

  <?php if(Session::has('flash_message')): ?>
                  
    <?php /*  Modal to display a message that the vehicle is deleted */ ?>
    <?php echo $__env->make('modals.vehicleIsDeleted', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <?php endif; ?>


  <?php echo e(display_errors($errors)); ?>

    
  <div class="panel panel-default">
    <div class="panel-heading">Edit Vehicle : Number <?php echo e($vehicle->id); ?> </div>
      <div class="panel-body">

        <?php /* Show delete button if the vehicle is not already marked as deleted */ ?>
        <?php if($vehicle->isDeleted == false): ?>
          <form method="post" action="/car/<?php echo e($vehicle->id); ?>/delete">
            <?php echo e(method_field('PATCH')); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="isDeleted" value="1">
            <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".confirmDelete">Delete Vehicle</button>
            <br><br>
            <?php /* Confirmation modal */ ?>
            <?php echo $__env->make('modals.confirmVehicleDelete', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          </form>
        <?php endif; ?>
        
        <?php echo $__env->make('forms.editVehicleForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </div>
  </div>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
<script type="text/javascript">
  $(document).ready(function () {
      $('#confirmModal').modal('show');
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>