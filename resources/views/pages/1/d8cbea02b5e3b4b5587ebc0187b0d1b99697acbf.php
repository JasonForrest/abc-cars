<?php echo $__env->make('parts.htmlHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('parts.topnav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div id="main-content" class="container">
            <div class="row">
                <div class="col-md-12 content">                
                    <div>

                        <?php echo $__env->yieldContent('title'); ?>

                    </div>
                    
                    <div>

                        <?php echo $__env->yieldContent('content'); ?>

                    </div>
                </div>
               
                
            </div>
        </div><!-- close #main-content -->
        <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="<?php echo e(url('/')); ?>">Home</a> - 
                    <a href="<?php echo e(url('about')); ?>">About</a> - 
                    <a href="<?php echo e(url('contact')); ?>">Contact</a> - 
                    <a href="<?php echo e(url('privacy')); ?>">Privacy</a> - 
                    <a href="<?php echo e(url('terms')); ?>">Terms & Conditions</a>
                <p>Copyright &copy; <?php echo e(date('Y')); ?> Jason Forrest</p>
            </div>
        </div>
        <?php echo $__env->make('parts.globalFooterScripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('footer-scripts'); ?>
    </body>
</html>
 
