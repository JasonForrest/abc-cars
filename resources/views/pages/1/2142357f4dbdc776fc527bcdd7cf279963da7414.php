<h2>Recently listed vehicle</h2>
<?php foreach($latestVehicle as $latestVehicle): ?>
<div class="col-md-8">
	<ul class="list-unstyled">
		<a href="car/<?php echo e($latestVehicle->id); ?>"><?php echo e(img_if_data_exists($latestVehicle->img, 'img-responsive', 'recently listed')); ?></a>
		<?php echo e(li_if_data_exists($latestVehicle->description, 'Details')); ?>

	<!-- Display edit vehicle button to 'staff' and 'admin' but NOT to 'user' -->
	</ul>
</div>

<div class="col-md-4">
	<ul class="list-unstyled">
		<li><strong>Make:</strong> <?php echo e($latestVehicle->vehicleModel->make); ?></li>
		<li><strong>Model:</strong> <?php echo e($latestVehicle->vehicleModel->model); ?></li>
		<li><strong>Transmission:</strong> <?php echo e($latestVehicle->transmission); ?></li>
		<li><strong>Mileage:</strong> <?php echo e($latestVehicle->odometer); ?> km</li>	
		<?php echo e(li_if_data_exists($latestVehicle->colour, 'Body colour')); ?>

		<?php echo e(li_if_data_exists($latestVehicle->vehicleCondition, 'Vehicle Condition')); ?>

		<?php echo e(li_if_data_exists($latestVehicle->kmPltrFuel, 'Fuel Economy - km/L')); ?>

		<?php echo e(li_if_data_exists($latestVehicle->serviceHistory, 'Service History')); ?>

		<li><strong>Price:</strong> $<?php echo e($latestVehicle->price); ?></li>	
	</ul>
</div>
<?php endforeach; ?>