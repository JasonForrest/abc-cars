<nav class="navbar">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-toggle" aria-expanded="false">
        
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="mobile-toggle">

        <ul class="nav navbar-nav">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li><a href="<?php echo e(url('about')); ?>">About</a></li>
            <li><a href="<?php echo e(url('contact')); ?>">Contact</a></li>
            <li><a href="<?php echo e(url('new-cars')); ?>">New Cars</a></li>
            <li><a href="<?php echo e(url('used-cars')); ?>">Used Cars</a></li>
        </ul>
      
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            
            <!-- Authentication Links -->
            <?php if(Auth::guest()): ?>
                <li><a href="<?php echo e(url('/login')); ?>">Login</a></li>
                <li><a href="<?php echo e(url('/register')); ?>">Register</a></li>
            <?php else: ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <?php echo e(Auth::user()->firstName); ?> <?php echo e(Auth::user()->lastName); ?><span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                    <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('access_dashboard')): ?>
                        <li><a href="<?php echo e(url('dashboard/staff')); ?>">Admin Dashboard</a></li>
                    <?php endif; ?>
                        <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
            <?php endif; ?>
            <form class="navbar-form navbar-left" method="GET" action="/search">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <input class="form-control" type="search" name="q" placeholder="Search" required>
                </div>
                <button type="submit" class="btn btn-small btn-primary">Search</button>
            </form>
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav><!-- close #navigation -->