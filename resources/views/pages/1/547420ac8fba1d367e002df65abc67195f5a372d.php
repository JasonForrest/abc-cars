<ul class="list-unstyled">
	<li><strong>Make:</strong> <?php echo e($car->vehicleModel->make); ?></li>
	<li><strong>Model:</strong> <?php echo e($car->vehicleModel->model); ?></li>
	<li><strong>Transmission:</strong> <?php echo e($car->transmission); ?></li>
	<li><strong>Mileage:</strong> <?php echo e($car->odometer); ?> km</li>	
	<?php echo e(li_if_data_exists($car->colour, 'Body colour')); ?>

	<?php echo e(li_if_data_exists($car->vehicleCondition, 'Vehicle Condition')); ?>

	<?php echo e(li_if_data_exists($car->kmPltrFuel, 'Fuel Economy')); ?>

	<?php echo e(li_if_data_exists($car->serviceHistory, 'Service History')); ?>

	<li><strong>Price:</strong> $<?php echo e($car->price); ?></li>	
	<div class="btn-group-vertical" role="group" aria-label="...">
		
			<a class="btn btn-primary" href="/abccars/public/enquiry/<?php echo e($car->id); ?>">Enquire</a>
			<a class="btn btn-primary" href="/abccars/public/order/<?php echo e($car->id); ?>">Place Order</a>
	
	<?php /* Display edit vehicle button to 'staff' and 'admin' but NOT to 'user'. See helpers.php to examin the isAdmin helper function */ ?>
	
			<?php /*The edit button*/ ?>
		<?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('edit_vehicle')): ?>
			<a class="btn btn-primary" href="<?php echo e($car->id); ?>/edit">Edit vehicle</a>
			
				<?php /*Just make sure the vehicle is not flagged as deleted. We don't want to even show the delete button if so*/ ?>
			<?php if(!$car->isDeleted): ?>
				
					<form method="post" action="/abccars/public/car/<?php echo e($car->id); ?>/delete">
						<?php echo e(method_field('PATCH')); ?>

						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="isDeleted" value="1">
					
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".confirmDelete">Delete Vehicle</button>

						<?php /*Display a modal to confirm deletion*/ ?>
						<?php echo $__env->make('modals.confirmVehicleDelete', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

					</form>
					
			<?php endif; ?>	
		<?php endif; ?>
		</div><?php /*end of button button group*/ ?>
	</div>
</ul>