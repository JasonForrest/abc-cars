<?php echo $__env->make('parts.htmlHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('parts.topnav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 


<?php /* Only include the banner section scripts if it exists in the view */ ?>
<?php if (! empty(trim($__env->yieldContent('banner')))): ?>
    <div class="container-fluid">
        <?php echo $__env->yieldContent('banner'); ?>
    </div>
<?php endif; ?>    
<?php echo $__env->yieldContent('title-row'); ?> 
    <div id="main-content" class="container">
        <div class="row">
            <div class="col-md-12 content">                
                
                
                <div id="content">

                    <?php echo $__env->yieldContent('content'); ?>
                    
                </div>
            </div>
           
        </div>
    </div><!-- close #main-content -->
    
    <?php /* Only include the lower content section scripts if it exists in the view */ ?>
<?php if (! empty(trim($__env->yieldContent('lower-content')))): ?>
    <div id="lower-content">
        <div class="container">
            <div class="row lc">
                    <?php echo $__env->yieldContent('lower-content'); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

   <div id="footer" class="sticky-footer">
            <div class="container text-center">
                <br/>
                <p>
                    <a href="<?php echo e(url('/')); ?>">Home</a> - 
                    <a href="<?php echo e(url('about')); ?>">About</a> - 
                    <a href="<?php echo e(url('contact')); ?>">Contact</a> - 
                    <a href="<?php echo e(url('privacy')); ?>">Privacy</a> - 
                    <a href="<?php echo e(url('terms')); ?>">Terms & Conditions</a>
                <p>Copyright &copy; <?php echo e(date('Y')); ?> Jason Forrest</p>
            </div>
        </div>

    <?php /* include any javascript required on all or most pages */ ?>
    <?php echo $__env->make('parts.globalFooterScripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php /* Only include the footer scripts if the section exists in the view */ ?>
    <?php if (! empty(trim($__env->yieldContent('footer-scripts')))): ?>
       <?php echo $__env->yieldContent('footer-scripts'); ?>
    <?php endif; ?>

    </body>
</html>
 
