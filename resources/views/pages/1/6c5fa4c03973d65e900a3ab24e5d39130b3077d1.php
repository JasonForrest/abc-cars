<?php $__env->startSection('title'); ?>
	<h1>User Dashboard</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
	<?php if($errors): ?>
		<?php foreach($errors as $error): ?>
			<?php echo e($errors); ?>

		<?php endforeach; ?>
	<?php endif; ?>

	<?php if(Session::has('flash_message')): ?>
                
        <div class="alert alert-info"><?php echo e(Session::get('flash_message')); ?></div>

    <?php endif; ?>
	
	
	<div class="col-md-9">
    
	    <?php foreach($user as $user): ?>
		   	<?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('administer_site')): ?>
		   	<div class="panel panel-default">
		   	
		    	<div class="panel-heading">
		    		Change User Role: <?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?> - <?php echo e(isset($user->label) ? $user->label : 'No role assigned'); ?>

		    	</div>
		     	
		     	<div class="panel-body"> 
		   				
	   				<form id="change-role" method="POST" action="/abccars/public/dashboard/staff/update-member-role/<?php echo e($user->userid); ?>">
				   		<?php echo e(method_field('PATCH')); ?>

					   	<?php echo e(csrf_field()); ?>

				   		<select name="role" class="form-control">
				   			<?php foreach($roles as $role): ?>
				   			<option value = "<?php echo e($role->id); ?>" 
					   			
					   			<?php echo e(selected($user->role_id, $role->id)); ?>

					   			><?php echo e($role->label); ?></option>			   			

				   			<?php endforeach; ?>
				   		</select>
				   		<input type="submit" class="btn btn-primary" name="submit">
				   </form>
				</div>
			</div>
		<?php endif; ?>


	   	<div class="panel panel-default">
		    <div class="panel-heading">Edit Member: <?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?> </div>
		      <div class="panel-body"> 
			   	<form id="edit-user" method="POST" action="/dashboard/staff/update-member/<?php echo e($user->id); ?>">
				   <?php echo e(method_field('PATCH')); ?>

				   <?php echo e(csrf_field()); ?>

				   	<div class="form-group">
					   	<label for="firstName">First Name</label>
					   	<input class="form-control" type="text" name="firstName" id="firstName" value="<?php echo e($user->firstName); ?>">
					</div>
				   
				   <div class="form-group">
					   	<label for="lastName">Last Name</label>
					   	<input class="form-control" type="text" name="lastName" id="lastName" value="<?php echo e($user->lastName); ?>">
					</div>

					<div class="form-group">
					   	
					   	<input class="form-control" type="hidden" name="email" id="email" value="<?php echo e($user->email); ?>">
					</div>

				   	<div class="form-group">
					   	<label for="phone">Phone</label>
					   	<input class="form-control" type="text" name="phone" id="phone" value="<?php echo e($user->phone); ?>">
					</div>
				   	

				   	<div class="form-group">
					   	<label for="streetNo">Street Number</label>
					   	<input class="form-control" type="text" name="streetNo" id="streetNo" value="<?php echo e($user->streetNo); ?>">
				   	</div>

				   	<div class="form-group">
					   	<label for="streetName">Street</label>
					   	<input class="form-control" type="text" name="streetName" id="streetName" value="<?php echo e($user->streetName); ?>">
				   	</div>

				   	<div class="form-group">
					   	<label for="suburb">Suburb</label>
					   	<input class="form-control" type="text" name="suburb" id="suburb" value="<?php echo e($user->suburb); ?>">
				   	</div>

				   	<div class="form-group">
					   	<label for="state">State</label>
					   	<input class="form-control" type="text" name="state" id="state" value="<?php echo e($user->state); ?>">
				   	</div>

				   	<div class="form-group">
					   	<label for="postcode">Postcode</label>
					   	<input class="form-control" type="text" name="postcode" id="postcode" value="<?php echo e($user->postCode); ?>">
				   	</div>

				   	<div class="form-group">
					   	<input class="btn btn-primary" type="submit" name="submit" id="submit">
				   	</div>
			   	</form>
		   	</div>
			</div>
		</div>
    <?php endforeach; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>