<?php $__env->startSection('title'); ?>
	<h1><?php echo e($supplier->name); ?></h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
	<h2>Supplier details</h2>
	

	<ul class="list-unstyled">
		
		<li><strong>Name</strong>: <?php echo e($supplier->name); ?></li>
		<li><strong>Abn</strong>: <?php echo e($supplier->abn); ?></li>
		<li><strong>Phone</strong>: <?php echo e($supplier->phone); ?></li>
		<li><strong>Email</strong>: <?php echo e($supplier->email); ?></li>
		<li><strong>Address</strong>: <?php echo e($supplier->streetNo); ?> <?php echo e($supplier->streetName); ?>, <?php echo e($supplier->suburb); ?> <?php echo e($supplier->state); ?> <?php echo e($supplier->postcode); ?></li>
		
	</ul>
	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>