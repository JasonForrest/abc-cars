<!--This view will display car/car->id-->


<?php $__env->startSection('title'); ?>
	<h1>Suppliers</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<table class="table table-responsive">
		<thead>
			<th>Name</th>
			<th>Address</th>
			<th>Phone</th>
			<th>Email</th>
			<th>ABN</th>
		</thead>
		<tbody>
	<?php foreach($suppliers as $supplier): ?>
			<tr>
				<td>
					<?php echo e($supplier->name); ?>

				</td>
				<td>
					<?php echo e($supplier->streetNo); ?> <?php echo e($supplier->streetName); ?> <?php echo e($supplier->suburb); ?> <?php echo e($supplier->state); ?> <?php echo e($supplier->postcode); ?>

				</td>
				<td>
					<?php echo e($supplier->phone); ?>

				</td>
				<td>
					<?php echo e($supplier->email); ?>

				</td>
				<td>
					<?php echo e($supplier->abn); ?>

				</td>
			</tr>
	<?php endforeach; ?>
		</tbody>
	</table>
	<?php echo e($suppliers->links()); ?>

	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>