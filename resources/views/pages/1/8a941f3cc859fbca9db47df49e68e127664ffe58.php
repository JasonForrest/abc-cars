<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js" type="text/javascript"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            $('#myTabs a').click(function (e) {
              e.preventDefault()
              $(this).tab('show')
            });
        </script>
        <script src="/js/stickyfooter/jquery.stickyfooter.min.js"></script>
        <script>
            $(window).load(function() {
                $("#footer").stickyFooter({
                    // The class that is added to the footer.
                    class: 'sticky-footer'
                });
            });
        </script>