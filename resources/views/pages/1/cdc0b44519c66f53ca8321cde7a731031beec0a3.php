<?php 
use App\VehicleModel;
use App\Supplier;
$vehicles = vehicleModel::all(); 
$suppliers = Supplier::all();
?>

<form method="POST" action="../<?php echo e($vehicle->id); ?>" enctype="multipart/form-data">
			<?php echo e(method_field('PATCH')); ?>

		<?php echo e(csrf_field()); ?>

			<div class="row">
				
				<div class="col-md-3">
					<div class="form-group">
						<label for="modelID">Model
							<select class="form-control" name="modelID">
								<option value="">-- Select Model --</option>
  							<?php foreach( $vehicles as $vehicleModel): ?>
  									<option value="<?php echo e($vehicleModel->id); ?>"	<?php echo e(selected($vehicle->modelID, $vehicleModel->id)); ?>> 	<?php echo e($vehicleModel->year); ?> <?php echo e($vehicleModel->make); ?> <?php echo e($vehicleModel->model); ?>

  									</option>
  							<?php endforeach; ?>
							</select>
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="supplier">Supplier
							<select class="form-control" name="supplier">
								<option value="">-- Select Supplier --</option>
  							<?php foreach( $suppliers as $supplier): ?>
  							
  								<option value="<?php echo e($supplier->id); ?>" <?php echo e(selected($vehicle->supplier, $supplier->id)); ?>>
  									<?php echo e($supplier->name); ?> 
								</option>
  							<?php endforeach; ?>
							</select>
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="odometer">Mileage
							<input class="form-control" id="odometer" name="odometer" type="text" value="<?php echo e($vehicle->odometer); ?>">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="transmission">Transmission
							<select id="transmission" class="form-control" name="transmission">
								<option value="">-- Select Transmission --</option>
								<option value="Automatic" <?php echo e(selected($vehicle->transmission, 'Automatic')); ?>>Automatic</option>
								<option value="Manual" <?php echo e(selected($vehicle->transmission, 'manual')); ?>>Manual</option>  							
							</select>
						</label>
					</div>
				</div>
			</div><!-- end form row -->

			<div class="row">

				<div class="col-md-3">
					<div class="form-group">
						<label for="colour">Body Colour
							<input class="form-control" id="colour" name="colour" type="text" value="<?php echo e($vehicle->colour); ?>">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="vehicleCondition">Vehicle Condition
							<input class="form-control" id="vehicleCondition" name="vehicleCondition" type="text" value="<?php echo e($vehicle->vehicleCondition); ?>">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="price">Price
							<input class="form-control" id="price" name="price" type="text" value="<?php echo e($vehicle->price); ?>">
						</label>
					</div>
				</div>

				
			</div><!-- end form row -->

			<div class="row">

				<div class="col-md-3">
					<label for="isSold">Is Sold?
						<select class="form-control" id="isSold" name="isSold">
							<option value="1" <?php echo e(selected($vehicle->isSold, 1)); ?>>Yes</option>
							<option value="0" <?php echo e(selected($vehicle->isSold, 0)); ?>>No</option>
						</select>
					</label>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="kmPltrFuel">Fuel Ecomomy km/L
							<input class="form-control" id="kmPltrFuel" name="kmPltrFuel" type="text" value="<?php echo e($vehicle->kmPltrFuel); ?>">
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="serviceHistory">Service History
							<textarea class="form-control" id="serviceHistory" name="serviceHistory"><?php echo e($vehicle->serviceHistory); ?></textarea>							
						</label>
					</div>
				</div>

				<div class="col-md-3">
					<label for="isFeatured">Is the vehicle featured?
						<select class="form-control" id="isFeatured" name="isFeatured">
							<option value="1" <?php echo e(selected($vehicle->isFeatured, 1)); ?>>Yes</option>
							<option value="0" <?php echo e(selected($vehicle->isFeatured, 0)); ?>>No</option>
						</select>
					</label>
				</div>
			</div><!-- end form row -->
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="form-group">
							<label for="description">Description
								<textarea class="form-control" id="description" name="description"><?php echo e($vehicle->description); ?></textarea>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="img">Image
					<input type="file" name="img">
				</label>
			</div>
			<input class="btn btn-primary" id="submit" name="submit" type="submit" value="Update">
		</form>


		