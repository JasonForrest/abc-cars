<?php $__env->startSection('title'); ?>
    <h1>User Dashboard</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
    <div class="row">
    <?php if(Session::has('flash_message')): ?>
                
        <div class="alert alert-info"><?php echo e(Session::get('flash_message')); ?></div>

    <?php endif; ?>
        <div class="col-md-9">
        <form method="GET">
            <div class="form-group">
                <input class="form-control" type="search" name="q" placeholder="Lookup user by name">
            </div>
        </form>
        
        <h4>Member List</h4>
        <?php if($users): ?>
            <table class="table table-responsive">
                <tbody>

                <?php foreach($users as $user): ?>
                    <tr>
                        <td>
                            <?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?>

                        </td>      
                        <td>
                            <a class="btn btn-primary pull-right" href="edit-member/<?php echo e($user->id); ?>">Edit</a>
                        </td> 
                        <td> 
                            <form id="delete-member" method="POST" action="delete-member/<?php echo e($user->id); ?>">
                                <?php echo e(method_field('PATCH')); ?>

                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" value="<?php echo e($user->id); ?>" name="id" >
                            
                            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".confirmDelete">Delete</button>
            <br><br>
            <?php /* Confirmation modal */ ?>
           
                        </td>
                    </tr>  
                <?php endforeach; ?>

                </tbody>
            </table>
        <?php endif; ?>
        <?php echo e($users->links()); ?>

        </div>

    </div>
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-scripts'); ?>
<?php echo $__env->make('modals.confirmMemberDelete', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
  $(document).ready(function () {
      $('#confirmModal').modal('show');
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>