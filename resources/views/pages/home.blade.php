@extends('layouts.landingPage')
@section('title-row')


<div class="container-fluid" style="background-color:#999;margin:2rem 0;">
<div class="container">
<div class="row">
    <div class="col-md-2">
        <img src="/img/abccars.png" alt="logo">
    </div>
    <div class="col-md-2 col-md-offset-6 social-icons pull-right" style="padding-top:1.5rem">
        <i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i>
        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
        <i class="fa fa-2x fa-google-plus-square" aria-hidden="true"></i>
        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
    </div>
</div>
</div>
</div>
@stop
@section('banner')
    <img class="image-responsive" src="img/banner1.jpg" width="100%">
@stop



@section('content')

    {{-- Display a modal with a session flash message if an unauthorised user has tried to access a restricted area--}}
    @if (Session::has('flash_message'))
                
        @include('modals.restrictedArea')

    @endif
	@if($singleFeatured)
    <div class="col-md-8">
        <img class="img-responsive" src="{{ $singleFeatured->img }}">
        
        <h1>{{ $singleFeatured->vehicleModel->year }} {{ $singleFeatured->vehicleModel->make }} {{ $singleFeatured->vehicleModel->model }}</h1>
        <ul class="list-unstyled">
            {{li_if_data_exists($singleFeatured->colour, 'Body colour')}}
            {{li_if_data_exists($singleFeatured->vehicleCondition, 'Vehicle Condition')}}
            {{li_if_data_exists($singleFeatured->kmPltrFuel, 'Fuel Economy')}}
            {{li_if_data_exists($singleFeatured->serviceHistory, 'Service History')}}
            {{li_if_data_exists($singleFeatured->description, 'Details')}}
        </ul>
    </div>
    @endif
    @if($latestFeaturedVehicles)
    <div class="col-md-4">
        <h4>Recently listed</h4>
        <ul class="list-unstyled">
        @foreach($latestFeaturedVehicles as $vehicle)
            
            <li><a href="car/{{ $vehicle->id }}">{{$vehicle->vehicleModel->year}} {{$vehicle->vehicleModel->make}} {{$vehicle->vehicleModel->model}}</a></li>

        @endforeach
        <ul>
    </div>
    @endif
	
                    
@stop

@section('lower-content')
    <p>Belay topsail Pieces of Eight quarterdeck Buccaneer long boat loot jolly boat Letter of Marque yawl. To go on account Yellow Jack Shiver me timbers haul wind case shot reef sails rutters pillage lugsail Brethren of the Coast. Clap of thunder provost Jack Ketch fluke pressgang Gold Road hardtack hang the jib Buccaneer cog.</p>
    
    <p>Rope's end cutlass weigh anchor Davy Jones' Locker pillage rum Brethren of the Coast parley coffer loot. Cat o'nine tails matey brigantine booty coffer Chain Shot scurvy boatswain Sail ho poop deck. Provost red ensign lugger sheet fluke barkadeer weigh anchor bilged on her anchor league spirits.</p>
    
    <p>Hornswaggle deadlights killick Sail ho gabion coffer Gold Road nipperkin boatswain belay. Jolly boat hang the jib provost topmast lugger hulk American Main coffer ye list. Belaying pin pressgang coxswain jury mast fire in the hole measured fer yer chains heave to lee cackle fruit square-rigged.</p>
@stop

@section('footer-scripts')

    <script type="text/javascript">
    $(document).ready(function () {

        $('#restrictedArea').modal('show');

    });
    </script>

@stop