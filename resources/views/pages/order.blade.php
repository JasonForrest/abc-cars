@extends('layouts.app')

@section('title')

{{-- 
Because the user must be logged in to enquire about a vehicle, we can simply send the logged in user name and email to the controller. 

Enquiries will be stored in the database and available for staff to view and reply to in the dashboard. In reality there could be some kind of alert system in place. But that is beyond the scope of this exercise.

I'll also send the vehicle details in the background based on the id of the vehicle that was clicked.

So the only fillable field in this case will be the actual enquiry itself. 

Nice and eay for the user and easy enough for me :)

 --}}
	<h1>Vehicle Order</h1>


	@if (Session::has('flash_message'))
                
            <div class="flash alert alert-warning"> 
                {{Session::get('flash_message')}} 
            </div>
            
    @endif


	<h4>Vehicle order from {{ Auth::user()->name }}</h4>
	<h4>Vehicle: {{ $vehicle->vehicleModel->year }} {{ $vehicle->vehicleModel->make }} {{ $vehicle->vehicleModel->model }}</h4>
	
	{{img_if_data_exists($vehicle->img)}}
@stop

@section('content')
{{ display_errors($errors) }}
	@include('forms.vehicleOrderForm')
	<br>
@stop

