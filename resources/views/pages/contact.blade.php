@extends('layouts.app')



@section('content')
@if(Session::has('message'))
    <div class="alert alert-info">
      {{Session::get('message')}}
    </div>
@endif
<div class="panel panel-default contact">
	<div class="panel-heading">Contact Us</div>
		<div class="panel-body">

			{{-- <form role="form" method="POST" action="{{ url('/contact') }}">
                
                {{ csrf_field() }}

				<div class="form-group">
			    	<label for="contactName">Your name</label>
			    	<input type="contactName" class="form-control" id="contactName" placeholder="Name">
			 	</div>
				<div class="form-group">
					<label for="email">Email address</label>
					<input type="email" class="form-control" id="email" placeholder="Email">
				</div>
				<div class="form-group">
					<label for="contactMessage">Your Message</label>
					<textarea class="form-control" rows="3" name="contactMessage" id="contactMessage"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>

			</form> --}}

		


		<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}

<div class="form-group">
    {!! Form::label('Your Name') !!}
    {!! Form::text('name', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your name')) !!}
</div>

<div class="form-group">
    {!! Form::label('Your E-mail Address') !!}
    {!! Form::text('email', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your e-mail address')) !!}
</div>

<div class="form-group">
    {!! Form::label('Your Message') !!}
    {!! Form::textarea('message', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your message')) !!}
</div>

<div class="form-group">
    {!! Form::submit('Contact Us!', 
      array('class'=>'btn btn-primary')) !!}
</div>
{!! Form::close() !!}
</div>

</div>

@stop

{{-- @section('sidebar')
	<h4>Recently listed</h4>
    <ul>
	    @foreach($latestFeaturedVehicles as $vehicle)
	    	<li><a href="car/{{ $vehicle->id }}">{{$vehicle->vehicleModel->year}} {{$vehicle->vehicleModel->make}} {{$vehicle->vehicleModel->model}}</a></li>

	    @endforeach
    <ul>
@stop --}}