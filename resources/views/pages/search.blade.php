@extends('layouts.noSidebar')

@section('title')
	<h1>Search Results</h1>
	@can('edit_vehicle')
		<p>Search or browse for vehicles below then click edit to edit a vehicle</p>
		<p>Vehicles can also be deleted from the edit page</p>
	@endcan
@stop

@section('content')
	
	<form method="GET">
    	<div class="form-group">
            <input class="form-control" type="search" name="q" placeholder="Search again">
        </div>
    </form>
    
	@if($vehicles)
		<h2>We found the following vehicles matching your search</h2>

	    @foreach($vehicles as $vehicle)
	   
	    <ul class="list-group">
				<div class="col-md-4 car">
					<li class="list-group-item">
						<a href="car/{{ $vehicle->id }}"><strong>{{ $vehicle->year }} {{ $vehicle->make }} {{ $vehicle->model }}</strong>
							<br>
							<img class="img-responsive thumbnail" height="100" src="{{ $vehicle->img }}">
						</a>
						<br>
						<h4 style="text-align:right;">${{ $vehicle->price }}</h4>
					</li>
			
				@can('edit_vehicle')
					<li class="list-group-item">
						<a class="btn btn-primary pull-left" href="car/{{ $vehicle->id }}/edit">Edit vehicle</a>
						<br><br>
					</li>
				@endcan	
				
				</div>
			</ul>	

	    @endforeach
	   
	<div class="row">{{-- Display pagination links if more than 12 cars --}}
			<div class="col-md-6 col-md-offset-4">
				{{$vehicles->links()}}
			</div>
		</div>
	@else
		<h4>We don't have any vehicles matching your search. Try again!</h4>    
	@endif
@stop