<!-- Modal -->
<div class="modal fade" id="emailPlaceholder" tabindex="-1" role="dialog" aria-labelledby="emailPlaceholder" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="emailPlaceholder">Restricted Area</h2>

            </div>
            <div class="modal-body">
                <p>Clicking the replay button would usually take you to the email reply form. However that is beyond the scope of this exercise.</p>
                 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>