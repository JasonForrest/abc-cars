<!-- Modal -->
<div class="modal fade" id="invoicePlaceholder" tabindex="-1" role="dialog" aria-labelledby="invoicePlaceholder" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="invoicePlaceholder">Restricted Area</h2>

            </div>
            <div class="modal-body">
                <p>Clicking the create invoice button will create and store the invoice in the database and send the invoice to the buyer. The application and database is already setup to include this functionality. However it has not been implemented at this stage.</p>
                 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>