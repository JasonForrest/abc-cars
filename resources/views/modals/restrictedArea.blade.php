<!-- Modal -->
<div class="modal fade" id="restrictedArea" tabindex="-1" role="dialog" aria-labelledby="restrictedArea" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="restrictedArea">Restricted Area</h2>

            </div>
            <div class="modal-body">
                <p>{{ Session::get('flash_message') }}</p>
                <p>You have been returned to the home page.</p>
                 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>