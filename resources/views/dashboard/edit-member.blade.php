@extends('layouts.dashboard')

@section('title')
	<h1>User Dashboard</h1>
@stop

@section('content')
	@if($errors)
	@foreach($errors as $error)
	{{$errors}}
	@endforeach
	@endif

	@if (Session::has('flash_message'))
                
        <div class="alert alert-info">{{ Session::get('flash_message') }}</div>

    @endif
	<div>
	
	<div class="col-md-9">
    
    @foreach($user as $user)
	   	@can('administer_site')
	   	<div class="panel panel-default">
	   	
	    	<div class="panel-heading">Change User Role: {{ $user->firstName }} {{ $user->lastName }} - {{ $user->label or 'No role assigned'}}</div>
	     	 	<div class="panel-body"> 
	   				
	   				<form id="change-role" method="POST" action="/dashboard/staff/update-member-role/{{$user->userid}}">
				   		{{ method_field('PATCH') }}
					   	{{ csrf_field() }}
				   		<select name="role" class="form-control">
				   			@foreach($roles as $role)
				   			<option value = "{{$role->id}}" 
					   			
					   			{{selected($user->role_id, $role->id)}}
					   			>{{$role->label}}</option>
					   			

				   			@endforeach
				   		</select>
				   		<input type="submit" class="btn btn-primary" name="submit">
				   </form>
				</div>
		</div>
	@endcan

   <div class="panel panel-default">
    <div class="panel-heading">Edit Member: {{ $user->firstName }} {{ $user->lastName }} </div>
      <div class="panel-body"> 
	   	<form id="edit-user" method="POST" action="/dashboard/staff/update-member/{{$user->id}}">
		   {{ method_field('PATCH') }}
		   {{ csrf_field() }}
		   	<div class="form-group">
			   	<label for="firstName">First Name</label>
			   	<input class="form-control" type="text" name="firstName" id="firstName" value="{{ $user->firstName }}">
			</div>
		   
		   <div class="form-group">
			   	<label for="lastName">Last Name</label>
			   	<input class="form-control" type="text" name="lastName" id="lastName" value="{{ $user->lastName }}">
			</div>

			<div class="form-group">
			   	
			   	<input class="form-control" type="hidden" name="email" id="email" value="{{ $user->email }}">
			</div>

		   	<div class="form-group">
			   	<label for="phone">Phone</label>
			   	<input class="form-control" type="text" name="phone" id="phone" value="{{ $user->phone }}">
			</div>
		   	

		   	<div class="form-group">
			   	<label for="streetNo">Street Number</label>
			   	<input class="form-control" type="text" name="streetNo" id="streetNo" value="{{ $user->streetNo }}">
		   	</div>

		   	<div class="form-group">
			   	<label for="streetName">Street</label>
			   	<input class="form-control" type="text" name="streetName" id="streetName" value="{{ $user->streetName }}">
		   	</div>

		   	<div class="form-group">
			   	<label for="suburb">Suburb</label>
			   	<input class="form-control" type="text" name="suburb" id="suburb" value="{{ $user->suburb }}">
		   	</div>

		   	<div class="form-group">
			   	<label for="state">State</label>
			   	<input class="form-control" type="text" name="state" id="state" value="{{ $user->state }}">
		   	</div>

		   	<div class="form-group">
			   	<label for="postcode">Postcode</label>
			   	<input class="form-control" type="text" name="postcode" id="postcode" value="{{ $user->postCode }}">
		   	</div>

		   	<div class="form-group">
			   	<input class="btn btn-primary" type="submit" name="submit" id="submit">
		   	</div>
	   	</form>
   	</div>
	</div>
	</div>
   

    @endforeach
            
        </div>

@stop
