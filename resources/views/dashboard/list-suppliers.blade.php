<!--This view will display car/car->id-->
@extends('layouts.dashboard')

@section('title')
	<h1>Suppliers</h1>
@stop

@section('content')
	<table class="table table-responsive">
		<thead>
			<th>Name</th>
			<th>Address</th>
			<th>Phone</th>
			<th>Email</th>
			<th>ABN</th>
		</thead>
		<tbody>
	@foreach($suppliers as $supplier)
			<tr>
				<td>
					{{$supplier->name}}
				</td>
				<td>
					{{$supplier->streetNo}} {{$supplier->streetName}} {{$supplier->suburb}} {{$supplier->state}} {{$supplier->postcode}}
				</td>
				<td>
					{{$supplier->phone}}
				</td>
				<td>
					{{$supplier->email}}
				</td>
				<td>
					{{$supplier->abn}}
				</td>
			</tr>
	@endforeach
		</tbody>
	</table>
	{{$suppliers->links()}}
	
@stop
