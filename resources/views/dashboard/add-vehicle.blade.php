@extends('layouts.dashboard')



@section('title')
	<h1>Staff Dashboard</h1>
@stop

@section('content')
	
	{{ display_errors($errors) }}
	
	<div>
		
		<div class="panel panel-default">
            <div class="panel-heading"><h4>Add a vehicle</h4></div>
                <div class="panel-body">
					@include('forms.addVehicleForm')
				</div>
			</div>
		</div>

  	</div>

</div>
@stop
