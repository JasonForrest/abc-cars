@extends('layouts.dashboard')

@section('title')
	<h1>Staff Dashboard</h1>
@stop

@section('content')

  @if (Session::has('flash_message'))
                  
    {{--  Modal to display a message that the vehicle is deleted --}}
    @include('modals.vehicleIsDeleted')

  @endif


  {{ display_errors($errors) }}
  	
  <div class="panel panel-default">
    <div class="panel-heading">Edit Vehicle : Number {{ $vehicle->id }} </div>
      <div class="panel-body">

        {{-- Show delete button if the vehicle is not already marked as deleted --}}
        @if($vehicle->isDeleted == false)
          <form method="post" action="/car/{{ $vehicle->id }}/delete">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <input type="hidden" name="isDeleted" value="1">
            <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".confirmDelete">Delete Vehicle</button>
            <br><br>
            {{-- Confirmation modal --}}
            @include('modals.confirmVehicleDelete')
          </form>
        @endif
  			
        @include('forms.editVehicleForm')

		</div>
	</div>
  	
@stop
@section('footer-scripts')
<script type="text/javascript">
  $(document).ready(function () {
      $('#confirmModal').modal('show');
  });
</script>
@stop
