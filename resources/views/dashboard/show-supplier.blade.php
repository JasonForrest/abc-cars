
@extends('layouts.dashboard')

@section('title')
	<h1>{{ $supplier->name }}</h1>
@stop

@section('content')
	
	<h2>Supplier details</h2>
	

	<ul class="list-unstyled">
		
		<li><strong>Name</strong>: {{ $supplier->name }}</li>
		<li><strong>Abn</strong>: {{ $supplier->abn }}</li>
		<li><strong>Phone</strong>: {{ $supplier->phone }}</li>
		<li><strong>Email</strong>: {{ $supplier->email }}</li>
		<li><strong>Address</strong>: {{ $supplier->streetNo }} {{ $supplier->streetName }}, {{ $supplier->suburb }} {{ $supplier->state }} {{ $supplier->postcode }}</li>
		
	</ul>
	
@stop
