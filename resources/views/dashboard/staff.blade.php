@extends('layouts.dashboard')

@section('title')
	<h1>Staff Dashboard</h1>
@stop

@section('content')
	<h2>Enquiries</h2>
	<div class="table-responsive">
	<table class="table">

	<thead>
		<th>User</th>
		<th>Email</th>
		<th>Enquiry</th>
		<th>Vehicle ID</th>
		<th>Make and Model</th>
		<th></th>
	</thead>
	<tbody>
	@foreach($enquiries as $enquiry)
	<tr>
		<td>{{$enquiry->firstName}} {{$enquiry->lastName}}</td>
		<td>{{$enquiry->email}}</td>
		<td>{{$enquiry->enquiry}}</td>
		<td>{{$enquiry->id}}</td>
		<td>{{$enquiry->year}} {{$enquiry->make}} {{$enquiry->model}}</td></td>
		<td><button class="btn btn-primary" data-toggle="modal" data-target="#emailPlaceholder">Reply</button>
	</tr>
	@endforeach
	</tbody>
	</table>
  	</div>

  	<h2>Orders</h2>
	<div class="table-responsive">
	<table class="table">

	<thead>
		<th>User</th>
		<th>Email</th>
		<th>Vehicle ID</th>
		<th>Make and Model</th>
		<th></th>
	</thead>
	<tbody>
	@foreach($orders as $order)
	<tr>
		<td>{{$order->firstName}} {{$order->lastName}}</td>
		<td>{{$order->email}}</td>
		<td>{{$order->id}}</td>
		<td>{{$order->year}} {{$order->make}} {{$order->model}}</td></td>
		<td><button class="btn btn-primary" data-toggle="modal" data-target="#invoicePlaceholder">Create Invoice</button>
	</tr>
	@endforeach
	</tbody>
	</table>
  	</div>

@include('modals.emailPlaceholder')
@include('modals.invoicePlaceholder')

</div>
@stop
