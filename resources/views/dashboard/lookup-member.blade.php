@extends('layouts.dashboard')

@section('title')
	<h1>User Dashboard</h1>
@stop

@section('content')
	
	<div class="row">
	@if (Session::has('flash_message'))
                
        <div class="alert alert-info">{{ Session::get('flash_message') }}</div>

    @endif
    	<div class="col-md-9">
        <form method="GET">
            <div class="form-group">
                <input class="form-control" type="search" name="q" placeholder="Lookup user by name">
            </div>
        </form>
        
        <h4>Member List</h4>
        @if($users)
            <table class="table table-responsive">
                <tbody>

                @foreach($users as $user)
                    <tr>
                        <td>
                            {{$user->firstName}} {{$user->lastName}}
                        </td>      
                        <td>
                            <a class="btn btn-primary pull-right" href="edit-member/{{$user->id}}">Edit</a>
                        </td> 
                        <td> 
                            <form id="delete-member" method="POST" action="/dashboard/staff/delete-member/{{$user->id}}">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <input type="hidden" value="{{$user->id}}" name="id" >
                            
                            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".confirmDelete">Delete</button>
            <br><br>
            {{-- Confirmation modal --}}
           
                        </td>
                    </tr>  
                @endforeach

                </tbody>
            </table>
        @endif
        {{$users->links()}}
        </div>

    </div>
        
@stop

@section('footer-scripts')
@include('modals.confirmMemberDelete')
<script type="text/javascript">
  $(document).ready(function () {
      $('#confirmModal').modal('show');
  });
</script>
@stop