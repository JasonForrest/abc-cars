@extends('layouts.app')

@if (Session::has('flash_message'))
                
    @include('modals.restrictedArea')

@endif

@section('title')
	<h1>{{ $car->vehicleModel->year }} {{ $car->vehicleModel->make }} {{ $car->vehicleModel->model }}</h1>
	@if($car->isDeleted)<h4 class="alert alert-danger">Vehicle Deleted</h4>@endif
	@if($car->isSold)<h4 class="alert alert-danger">Vehicle SOLD</h4>@endif
@stop


@section('content')
	{{ img_if_data_exists($car->img, 'img-responsive', $car->vehicleModel->year.' '.$car->vehicleModel->make.' '.$car->vehicleModel->model) }}
	<p><strong>Details:</strong> {{ $car->description }}</p>
@stop

@section('sidebar')
	@include('parts.SidebarShowCar')	
@stop