@extends('layouts.app')

@section('title')
	<h1>New Cars</h1>
@stop

@section('featured')
@include('parts.recentlyListed')
@stop
@section('content')
	
	@if($newCars)
	@foreach($newCars as $car)

		<div class="col-md-4 col-sm-6 car">
					<a href="car/{{ $car->id }}"><strong>{{ $car->vehicleModel->year }} {{ $car->vehicleModel->make }} {{ $car->vehicleModel->model }}</strong>
						<br>
						<img class="img-responsive thumbnail" height="100" src="{{ $car->img }}">
					</a>
					
					@if($car->isSold)<h4 class="pull-left text-danger">Vehicle SOLD</h4>@endif<h4 style="text-align:right;">Price ${{ $car->price }}</h4>	
			
		</div>

	@endforeach
	@else
		<h4>There are no new cars available at the moment. Please check our used cars page.</h4>
	@endif
	<div class="row">
		<div class="col-md-6 col-md-offset-4">
			{{$newCars->links()}}
		</div>
	</div>
@stop