{{--This view will display a list of available models--}}
@extends('layouts.app')

@section('title')
	<h1>New Cars</h1>
@stop

@section('content')
	<p>The new cars page</p>
	<select name="cars">
			<option value="">-- Select Model --</option>
  		@foreach($vehicleModels as $vehicleModel)
  			<option value="{{ $vehicleModel->id }}">{{ $vehicleModel->year }} {{ $vehicleModel->make }} {{ $vehicleModel->model }}</option>
  		@endforeach
	</select>
@stop