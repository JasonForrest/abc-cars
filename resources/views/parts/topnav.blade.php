<nav class="navbar">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-toggle" aria-expanded="false">
        
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="mobile-toggle">

        <ul class="nav navbar-nav">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('about') }}">About</a></li>
            <li><a href="{{ url('contact') }}">Contact</a></li>
            <li><a href="{{ url('new-cars') }}">New Cars</a></li>
            <li><a href="{{ url('used-cars') }}">Used Cars</a></li>
        </ul>
      
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->firstName }} {{ Auth::user()->lastName }}<span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                    @can('access_dashboard')
                        <li><a href="{{ url('dashboard/staff') }}">Admin Dashboard</a></li>
                    @endcan
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
            @endif
            <form class="navbar-form navbar-left" method="GET" action="/search">
                {{ csrf_field() }}
                <div class="form-group">
                    <input class="form-control" type="search" name="q" placeholder="Search" required>
                </div>
                <button type="submit" class="btn btn-small btn-primary">Search</button>
            </form>
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav><!-- close #navigation -->