@unless (Auth::guest())

		@if(Auth::user()->accessLevel == 'staff' || Auth::user()->accessLevel == 'admin')
			<li>
			<a class="btn btn-primary pull-left" href="{{ $vehicle->id }}/edit">Edit vehicle</a>
			
			<form method="post" action="/car/{{ $vehicle->id }}/delete">
			{{ method_field('PATCH') }}
		{{ csrf_field() }}
			<input type="hidden" name="isDeleted" value="1">
			

<!-- Confirmation modal -->

<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".confirmDelete">Delete Vehicle</button>

<div class="modal fade confirmDelete" tabindex="-1" role="dialog" aria-labelledby="modal-title">
  <div class="modal-dialog modal-sm" role="document">
    	<div class="modal-content">
    		<div class="modal-header">
	    		<div class="modal-title">
	    			<h4>Please confirm</h4>
	    		</div>
	    	</div>
    		<div class="modal-body">
   				<p>Are you sure you want to delete?</p>
   			</div>
   			<div class="modal-footer">
      			<input class="btn btn-danger pull-right" type="submit" name="submit" value="Delete vehicle">
    
    			<button class="btn btn-primary pull-left"  data-dismiss="modal">No thanks</button>
    		</div>
  		</div>
	</div>
</div>
</form>
</li>		
	</ul>
		@endif

	@endunless