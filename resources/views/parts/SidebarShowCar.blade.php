<ul class="list-unstyled">
	<li><strong>Make:</strong> {{ $car->vehicleModel->make }}</li>
	<li><strong>Model:</strong> {{ $car->vehicleModel->model }}</li>
	<li><strong>Transmission:</strong> {{ $car->transmission }}</li>
	<li><strong>Mileage:</strong> {{ $car->odometer }} km</li>	
	{{ li_if_data_exists($car->colour, 'Body colour') }}
	{{ li_if_data_exists($car->vehicleCondition, 'Vehicle Condition') }}
	{{ li_if_data_exists($car->kmPltrFuel, 'Fuel Economy') }}
	{{ li_if_data_exists($car->serviceHistory, 'Service History') }}
	<li><strong>Price:</strong> ${{ $car->price }}</li>	
	<div class="btn-group-vertical" role="group" aria-label="...">
		
			<a class="btn btn-primary" href="/enquiry/{{ $car->id }}">Enquire</a>
			<a class="btn btn-primary" href="/order/{{ $car->id }}">Place Order</a>
		
	{{-- Display edit vehicle button to 'staff' and 'admin' but NOT to 'user'. See helpers.php to examin the isAdmin helper function --}}
	
			{{--The edit button--}}
		@can('edit_vehicle')
			<a class="btn btn-primary" href="{{ $car->id }}/edit">Edit vehicle</a>
			
				{{--Just make sure the vehicle is not flagged as deleted. We don't want to even show the delete button if so--}}
			@if(!$car->isDeleted)
				
					<form method="post" action="/car/{{ $car->id }}/delete">
						{{ method_field('PATCH') }}
						{{ csrf_field() }}
						<input type="hidden" name="isDeleted" value="1">
					
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".confirmDelete">Delete Vehicle</button>

						{{--Display a modal to confirm deletion--}}
						@include('modals.confirmVehicleDelete')

					</form>
					
			@endif	
		@endcan
		</div>{{--end of button button group--}}
	</div>
</ul>