<h2>Recently listed vehicle</h2>
@foreach($latestVehicle as $latestVehicle)
<div class="col-md-8">
	<ul class="list-unstyled">
		<a href="car/{{ $latestVehicle->id }}">{{ img_if_data_exists($latestVehicle->img, 'img-responsive', 'recently listed') }}</a>
		{{ li_if_data_exists($latestVehicle->description, 'Details') }}
	<!-- Display edit vehicle button to 'staff' and 'admin' but NOT to 'user' -->
	</ul>
</div>

<div class="col-md-4">
	<ul class="list-unstyled">
		<li><strong>Make:</strong> {{ $latestVehicle->vehicleModel->make }}</li>
		<li><strong>Model:</strong> {{ $latestVehicle->vehicleModel->model }}</li>
		<li><strong>Transmission:</strong> {{ $latestVehicle->transmission }}</li>
		<li><strong>Mileage:</strong> {{ $latestVehicle->odometer }} km</li>	
		{{ li_if_data_exists($latestVehicle->colour, 'Body colour') }}
		{{ li_if_data_exists($latestVehicle->vehicleCondition, 'Vehicle Condition') }}
		{{ li_if_data_exists($latestVehicle->kmPltrFuel, 'Fuel Economy - km/L') }}
		{{ li_if_data_exists($latestVehicle->serviceHistory, 'Service History') }}
		<li><strong>Price:</strong> ${{ $latestVehicle->price }}</li>	
	</ul>
</div>
@endforeach