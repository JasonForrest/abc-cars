<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable = [
    'enquiry',
    'userID',
    'vehicleID'
    ];

    public function enquiryVehicle()
	{
	return $this->hasOne('App\Vehicle', 'id', 'vehicleID');
	}

	public function enquiryUser()
	{
	return $this->hasOne('App\User', 'id', 'userID');
	}
}
