<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 
        'lastName',
        'email', 
        'password',
        'phone',
        'streetNo',
        'streetName',
        'suburb',
        'state',
        'postcode',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function purchase()
    {
        return $this->hasMany('App\Purchase', 'buyerId', 'id');
    }

    public function enquiry()
    {
        return $this->belongsTo('App\Enquiry', 'userID', 'id');
    }

     public function order()
    {
        return $this->belongsTo('App\Order', 'buyerId', 'id');
    }

    
}
