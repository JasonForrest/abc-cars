<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
   public function vehicle()
	{
	return $this->hasMany('App\Vehicle', 'modelID', 'id');
	}

}
