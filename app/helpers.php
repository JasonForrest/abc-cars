<?php

function flash($message, $level = 'info'){

	session()->flash('flash_message', $message);
	session()->flash('flash_message_level', $level);

}
function selected($dbValue, $formField){
	if($dbValue == $formField){
		echo "selected";
	}
}

function display_errors($errors){
	if(count($errors)){
	  	echo '<div class="alert alert-danger">
		  	<ul>';
		  		foreach ($errors->all() as $error){
		  			echo'<li>'.$error.' </li>';
		  		}
		  		
		  	echo'</ul>
	  	</div>';
  	}
}

function li_if_data_exists($data, $dataTitle){
	if(($data)){
	echo '<li><strong>'.$dataTitle.': </strong>'.$data.'</li>';
	}
}

function img_if_data_exists($path, $class="img-responsive", $alt="image"){
	if(($path)){
	echo '<img src="'.$path.'" class="'.$class.'" alt="'.$alt.'">';
	}
}