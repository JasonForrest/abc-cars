<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function User()
	{
	return $this->belongsTo('App\User', 'id', 'buyerId');
	}

	public function Vehicle()
	{
	return $this->belongsTo('App\Vehicle', 'id', 'vehicleId');
	}

	public function Invoice()
	{
	return $this->belongsTo('App\Invoice', 'purchaseId', 'id');
	}
}
