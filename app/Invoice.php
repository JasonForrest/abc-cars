<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function purchase()
	{
	return $this->hasOne('App\Purchase', 'id', 'purchaseId');
	}
}
