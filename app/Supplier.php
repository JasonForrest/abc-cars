<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
	protected $fillable = [
		'name',
        'abn',
        'phone',
        'email',
        'streetNo',
        'streetName',
        'suburb',
        'state',
        'postcode',
        'notes'
	];
    
    public function vehicle()
	{
	return $this->hasMany('App\Vehicle', 'supplier', 'id');
	}
}
