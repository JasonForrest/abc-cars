<?php

namespace App\Http\Middleware;

use Closure;

class authenticateStaffMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user && $user->accessLevel == 'staff'){
            return $next($request);
        }
        
        //set a failure message and redirect to the login page if the user is not a staff member
        flash('Only staff members can access the dashboard');
        return redirect('/');
        
        
    }
}
