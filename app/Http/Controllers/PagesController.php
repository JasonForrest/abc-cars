<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Vehicle;
use App\VehicleModel;
use App\Supplier;


use DB;


        
class PagesController extends Controller
{
    
    public function home()
    {
        $singleFeatured = Vehicle::with('vehicleModel')
        ->where('isFeatured', '=', '1')
        ->where('isDeleted', '=', 0)
        ->first();

        $latestFeaturedVehicles = Vehicle::with('vehicleModel')
        ->where('isFeatured', '=', '1')
        ->where('isDeleted', '=', 0)
        ->take(5)
        ->get();
        
    	return view('pages.home', compact('singleFeatured'), compact('latestFeaturedVehicles'));
    }
    public function about()
    {
    	return view('pages.about');
    }

    public function privacy()
    {
        return view('pages.privacy');
    }

     public function terms()
    {
        return view('pages.terms');
    }

    public function search(Request $request)
    {
        
        $search = '%'.$request->q.'%';
        
        $vehicles = DB::table('vehicles')
            ->join('vehicle_models', 'modelID', '=', 'vehicle_models.id')
            ->select('vehicles.*', 'vehicle_models.make AS make', 'vehicle_models.model AS model', 'vehicle_models.year AS year')
            ->where( 'make', 'like', $search )
            ->orWhere( 'model', 'like', $search )
            ->orWhere( 'year', '=', $request->q )  
            ->where('isDeleted', '=', 0)       
            ->paginate(12);

       
        return view('pages.search', compact('vehicles'));
    }

    
   
   
}
