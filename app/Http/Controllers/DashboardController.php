<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth;
use Validator;

use Gate;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SupplierRequest;
use App\Http\Requests\SearchRequest;

use App\Vehicle;
use App\Supplier;
use App\Enquiry;
use App\User;
use App\Role;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        //firstly make sure the user is logged in
        $this->middleware('auth');
    }
    
    public function staff()
    {
        //only the Site Manager and Staff Members can access the dashboard
        if(Gate::allows('access_dashboard')){ 
           
            //get details of all enquiries
            $enquiries = DB::table('enquiries')
            ->join('users', 'enquiries.userID', '=', 'users.id')
            ->join('vehicles', 'enquiries.vehicleID', '=', 'vehicles.id')
            ->join('vehicle_models', 'vehicles.modelId', '=', 'vehicle_models.id')
            ->get();

            //get details of all orders
            $orders = DB::table('orders')
            ->join('users', 'orders.buyerId', '=', 'users.id')
            ->join('vehicles', 'orders.vehicleId', '=', 'vehicles.id')
            ->join('vehicle_models', 'vehicles.modelId', '=', 'vehicle_models.id')
            ->get();
            
            return view('dashboard.staff', compact('enquiries'), compact('orders'));
        }
        else
        {
            flash('You are not permitted to access the admin dashboard!');
            return redirect('/');
        }
    }

    
    public function addVehicle()
    {
        //only the Site Manager and Staff Members can access the dashboard
        if(Gate::allows('access_dashboard')){
    	   return view('dashboard.add-vehicle');
         }
            else
        {
            flash('You are not permitted to access the admin dashboard!');
            return redirect('/');
        }
    }
    
    public function deleteVehicle()
    {
        //only the Site Manager and Staff Members can access the dashboard
        if(Gate::allows('access_dashboard')){
    	   return view('dashboard.delete-vehicle');
        }
            else
        {
            flash('You shall not pass!');
            return redirect('/');
        }
    }
    public function addSupplier()
    {
        //only the Site Manager and Staff Members can access the dashboard
        if(Gate::allows('access_dashboard')){
        
            return view('dashboard.add-supplier');
        }
            else
        {
            flash('You shall not pass!');
            return redirect('/');
        }
    }

    public function supplierStore(SupplierRequest $request, Supplier $supplier)
    {       
        //only the Site Manager and Staff Members can access the dashboard
        if(Gate::allows('access_dashboard')){
                    
            $newSupplier = $supplier->create($request->all());

            $id =  $newSupplier->id;
            
            return redirect('/supplier/'.$id );
         }
            else
        {
            flash('You shall not pass!');
            return redirect('/');
        }
    }

    public function show(Supplier $supplier)
    {      

        //only the Site Manager and Staff Members can access the dashboard
        if(Gate::allows('access_dashboard')){ 

        return view('dashboard.show-supplier', compact('supplier'));
         }
            else
        {
            flash('You shall not pass!');
            return redirect('/');
        }
    }

   public function listSuppliers()
    {
        if(Gate::allows('access_dashboard')){            
            $suppliers = DB::table('suppliers')
                ->select('*')
                     
                ->paginate(10);
            return view('dashboard.list-suppliers', compact('suppliers'));
        } 
         else
        {
            flash('You shall not pass!');
            return redirect('/');  
        }
    }
}



