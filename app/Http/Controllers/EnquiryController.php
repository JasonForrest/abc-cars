<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\EnquiryRequest;

use App\Vehicle;
use App\VehicleModel;
use App\Supplier;
use App\Enquiry;

use DB;

class EnquiryController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function enquire(Vehicle $vehicle, VehicleModel $vehicleModel){
            
        return view('pages.enquiry', compact('vehicle'));
    }

    public function enquiryStore(EnquiryRequest $request, Vehicle $vehicle)
    {       
        $enquiry = new Enquiry;
        $newEnquiry = $enquiry->create($request->all());
    	flash('Thank you for your enquiry. We will contact you soon.');
		return back();
	}

	
}
