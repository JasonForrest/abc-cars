<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ContactFormRequest;

use App\Vehicle;
use App\VehicleModel;
use App\Supplier;

use DB;
use Mail;


class ContactController extends Controller
{
    /*public function contact($carCount = 6)
    {
        $latestFeaturedVehicles = Vehicle::with('vehicleModel')
        ->where('isFeatured', '=', '1')
        ->where('isDeleted', '=', 0)
        ->take($carCount)
        ->get();
    	return view('pages.contact', compact('latestFeaturedVehicles'));
    }*/
    
    public function create()
    {
        return view('pages.contact');
    }

    public function store(ContactFormRequest $request)
{
	
    Mail::send('emails.contact',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message')
        ), function($message)
    {
        $message->from('contact@abccars.com');
        $message->to('owner@abccars.com', 'Admin')->subject('Message from your website');

    });

  return \Redirect::route('contact')->with('message', 'Thanks for contacting us!');

}
}
