<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\VehicleRequest;

use App\Vehicle;
use App\VehicleModel;
use App\Supplier;

use DB;

class CarsController extends Controller
{
    public function usedCars()
    {
        //get the latest vehicle listed
        $latestVehicle = Vehicle::with('vehicleModel')
        ->where('isDeleted', '=', 0)
        ->where('vehicleCondition', '<>', 'new')
        ->where('vehicleCondition', '<>', 'New')
        ->orderBy('vehicles.created_at', 'DESC')
        ->take(1)
        ->get();

    	//get all used cars that are not flagged as deleted and return 12 per page
        $usedCars = Vehicle::with('vehicleModel')
        ->where('vehicleCondition', '<>', 'new')
        ->where('vehicleCondition', '<>', 'New')
        ->where('isDeleted', '=', 0)
        ->paginate(12);
        
    	//return the veiw and pass in arrays for the latest vehicle and all used cars
        return view('cars.used-cars', compact('usedCars'), compact('latestVehicle'));
    }

    public function newCars()
    {
    	//get the latest vehicle listed
        $latestVehicle = Vehicle::with('vehicleModel')
        ->where('isDeleted', '=', 0)
        ->where('vehicleCondition', '=', 'new')
        ->orWhere('vehicleCondition', '=', 'New')        
        ->orderBy('vehicles.created_at', 'DESC')
        ->take(1)
        ->get();

        //get all new cars that are not flagged as deleted and return 12 per page
    	$newCars = Vehicle::with('vehicleModel')
        ->where('vehicleCondition', '=', 'new')
        ->where('isDeleted', '=', 0)
        ->paginate(12);

        //return the veiw and pass in arrays for the latest vehicle and all new cars
    	return view('cars.new-cars', compact('newCars'), compact('latestVehicle'));
    }

    

    public function car()
    {
    	return view('cars.car');
    }

    public function show(Vehicle $car)
    {    	

    	return view('cars.show-car', compact('car'));
    }

    public function store(VehicleRequest $request)
    {       
        
        $input = $request->all();

        $file = $request->file('img');
        
        $car = new Vehicle;

        //first create/store the vehicle
        $newCar = $car->create([
        'modelID' => $request->modelID, 
        'supplierID' => $request->supplier, 
        'odometer' => $request->odometer, 
        'transmission' => $request->transmission, 
        'colour' => $request->colour, 
        'vehicleCondition' => $request->vehicleCondition, 
        'price' => $request->price, 
        'kmPltrFuel' => $request->kmPltrFuel, 
        'serviceHistory' => $request->serviceHistory, 
        'isSold' => $request->isSold, 
        'isFeatured' => $request->isFeatured, 
        'description' => $request->description, 
        'isDeleted' => 0
        ]);

        //get the id of the inserted vehicle
        $id = $newCar->id;

        //move the image to the correct directory
        $file->move('vehicle/photos/'.$id, time().$file->getClientOriginalName());

        //build the image file path and store it in the database
        $storeFilePath = '/vehicle/photos/'.$id.'/'.time().$file->getClientOriginalName();
        $newCar->update(['img' => $storeFilePath]);

        return redirect('/car/'.$id );
    }

    //Display the vehicle edit form
    public function edit(Request $request, Vehicle $vehicle)
    {
        return view('dashboard.edit-vehicle', compact('vehicle'));
    }

    public function deleteVehicle(Request $request, Vehicle $vehicle)
    {
        $vehicle->update($request->all());
        flash('Vehichle '. $vehicle->id .' has been marked as deleted and will no longer be displayed to visitors');
        return back();
    }

    //Update database record
    public function update(Request $request, Vehicle $vehicle)
    {
        
        $input = $request->all();
        
        $id = $vehicle->id;

        if($request->file('img'))
        {
            //If a new image has been upploaded go ahead and store it in the file system and database
            $file = $request->file('img');
            $file->move('vehicle/photos/'.$vehicle->id, time().$file->getClientOriginalName());
            $storeFilePath = '/vehicle/photos/'.$vehicle->id.'/'.time().$file->getClientOriginalName();

        } 
            else 
        {
            //otherewise just keep the image as it is
            $storeFilePath = $vehicle->img;
        }

        //update the details
        $update = $vehicle->update([
        'modelID' => $request->modelID, 
        'supplier' => $request->supplier, 
        'odometer' => $request->odometer, 
        'transmission' => $request->transmission, 
        'colour' => $request->colour, 
        'vehicleCondition' => $request->vehicleCondition, 
        'price' => $request->price, 
        'kmPltrFuel' => $request->kmPltrFuel, 
        'serviceHistory' => $request->serviceHistory, 
        'isSold' => $request->isSold, 
        'isFeatured' => $request->isFeatured, 
        'description' => $request->description, 
        'isDeleted' => 0,
        'img' => $storeFilePath
        ]);

        return redirect('/car/'.$vehicle->id );
    }

}
