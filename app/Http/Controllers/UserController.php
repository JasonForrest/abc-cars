<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Auth;
use Validator;

use Gate;

use App\Http\Requests\SupplierRequest;
use App\Http\Requests\SearchRequest;

use App\Vehicle;
use App\Supplier;
use App\Enquiry;
use App\User;
use App\Role;

use DB;

class UserController extends Controller
{
	
    public function lookupMember(Request $request, User $user)
    {
        if(Gate::allows('edit_user')){ 
            if($request)
            $search = '%'.$request->q.'%';
            
            $users = DB::table('users')
                ->select('id', 'firstName', 'lastName', 'suburb', 'state')
                ->where( 'firstName', 'like', $search ) 
                ->orWhere( 'lastName', 'like', $search )           
                ->paginate(10);
        	return view('dashboard.lookup-member', compact('users'));
            }
            else
            {
                flash('You shall not pass!');
                return redirect('/');
        }
    }

    public function editMember(Request $request)
    { 
        if(Gate::allows('edit_user')){
            $user = DB::table('users')
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('*', 'users.id as userid')
            ->where( 'users.id', '=', $request->id )
            ->get();
            $roles = DB::table('roles')            
            ->get();

        return view('dashboard.edit-member', compact('user'), compact('roles'));
        }
            else
            {
                flash('You shall not pass!');
                return redirect('/');
            }
    }




 	public function updateMember($id, Request $request)
	{ 
		if(Gate::allows('edit_user')){

            $user = User::findOrFail($id);            

           /* $roleUser = DB::table('role_user')->where('user_id', '=', $id)->findOrFail();

            if(!$roleUser)
                {
                    
                    DB::table('users')->insert(
                        [
                        'role_id' => 3, 
                        'user_id' => $id
                        ]);
                }
*/

		    $update = $user->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'phone' => $request->phone,
                'streetNo' => $request->streetNo,
                'streetName' => $request->streetName,
                'suburb' => $request->suburb,
                'state' => $request->state,
                'postcode' => $request->postcode
                ]);
                
            if($update){
		    flash('Member details updated!');
            
	    	return redirect( '/dashboard/staff/edit-member/'.$id );
		    	}
		    	else
		    	{
	    		flash('Error! Member details not updated!');
	    		return back();
	    	}
	    }
	    	else
	    {
	    	flash('You are not authorised to do that');
	    	return back();
	    }

	}

    public function updateMemberRole($id, Request $request)
    { 
        if(Gate::allows('edit_user')){
            //dd($request->role);

            //delete existing role
            $roleUser = DB::table('role_user')->where('user_id', '=', $id)->get();

            if($roleUser)
                {
                    DB::table('role_user')->where('user_id', '=', $id)->delete();
                }
            //lookup the user
            $user = User::findOrFail($id);

            //define role from request
            $role = Role::where( 'id', $request->role )->firstOrFail();

            //update role
            $save = $user->roles()->save($role);
               
            if($save){
            flash('Member role updated!');
            
            return back();
                }
                else
                {
                flash('Error! Member role not updated!');
                return back();
            }
        }
            else
        {
            flash('You are not authorised to do that');
            return back();
        }

    }


    public function deleteMember(Request $request)
    {
        if(Gate::allows('access_dashboard')){
    	
           DB::table('users')->where('id', '=', $request->id)->delete();

           flash('Member Deleted!');
           return back();

        }
            else
        {
            flash('You shall not pass!');
            return redirect('/');
        }
    }
   }