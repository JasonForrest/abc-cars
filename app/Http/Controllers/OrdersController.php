<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\OrdersRequest;

use App\Vehicle;
use App\VehicleModel;
use App\Supplier;
use App\Enquiry;
use App\Order;

use DB;

class OrdersController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function order(Vehicle $vehicle, VehicleModel $vehicleModel){
            
        return view('pages.order', compact('vehicle'));
    }

    public function orderStore(OrdersRequest $request, Vehicle $vehicle)
    {       
    	$order = new Order;
        $newOrder = $order->create($request->all());
    	flash('Thank you for your order. We will contact you soon.');
		return back();
	}

}
