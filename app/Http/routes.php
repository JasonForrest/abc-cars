<?php

//Page Routes
Route::get('/', 'PagesController@home');
Route::get('about', 'PagesController@about');
Route::get('privacy', 'PagesController@privacy');
Route::get('terms', 'PagesController@terms');

//Contact Form Routes
Route::get('contact', ['as' => 'contact', 'uses' => 'ContactController@create']);
Route::post('contact', ['as' => 'contact_store', 'uses' => 'ContactController@store']);
//Route::get('contact', 'PagesController@contact');

//Enquiry Routes
Route::get('/enquiry/{vehicle}', 'EnquiryController@enquire');
Route::post('enquiry/{id}', 'EnquiryController@enquiryStore');

//Order Routes
Route::get('/order/{vehicle}', 'OrdersController@order');
Route::post('order/{id}', 'OrdersController@orderStore');

//View cars routes
Route::get('used-cars', 'CarsController@usedCars');
Route::get('new-cars', 'CarsController@newCars');
Route::get('models', 'CarsController@listModels');
Route::get('car/{car}', 'CarsController@show');

//Create and update car routes
Route::post('addcar', 'CarsController@store');

Route::get('/car/{vehicle}/edit', 'CarsController@edit');
Route::patch('/car/{vehicle}/delete', 'CarsController@deleteVehicle');
Route::patch('/car/{vehicle}', 'CarsController@update');

//Staff/Admin dashboard routes
Route::get('dashboard/staff', 'DashboardController@staff');
Route::get('dashboard/staff/add-vehicle', 'DashboardController@addVehicle');
Route::get('dashboard/staff/add-supplier', 'DashboardController@addSupplier');
Route::get('dashboard/staff/list-suppliers', 'DashboardController@listSuppliers');
Route::post('addsupplier', 'DashboardController@supplierStore');
Route::get('supplier/{supplier}', 'DashboardController@show');

//staff/admin edit user routes
Route::get('/dashboard/staff/lookup-member', 'UserController@lookupMember');
Route::get('dashboard/staff/edit-member/{id}', 'UserController@editMember');
Route::patch('/dashboard/staff/update-member/{id}', 'UserController@updateMember');
Route::patch('/dashboard/staff/update-member-role/{id}', 'UserController@updateMemberRole');
Route::patch('/dashboard/staff/delete-member/{id}', 'UserController@deleteMember');
//search Routes
Route::get('search', 'PagesController@search');


Route::auth();

//Route::get('/home', 'HomeController@index');
