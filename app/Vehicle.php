<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
	//form fields that will be accepted. Fields not listed here will be ignored
	protected $fillable = [
		'modelID', 
		'supplierID', 
		'odometer', 
		'transmission', 
		'colour', 
		'vehicleCondition', 
		'price', 
		'dateAdded', 
		'kmPltrFuel', 
		'serviceHistory', 
		'isSold', 
		'isFeatured', 
		'description', 
		'img', 
		'q', 
		'isDeleted'
		];
	
	/* relationships */

	/* Relationship between the vehicle and the vehicle_model.
	** Each vehicle has only one model - vehicles.modelID references vehicle_models.id */
	public function vehicleModel()
	{
	return $this->hasOne('App\VehicleModel', 'id', 'modelID');
	}

	/** Relationship between the vehicle and the supplier. 
	** Each vehicle can have only one supplier - vehicles.supplier references supplier.id */
	public function vehicleSupplier()
	{
	return $this->hasOne('App\Supplier', 'id', 'supplier');
	}

	public function vehicleBuyer()
	{
	return $this->hasOne('App\Purchase', 'vehicleID', 'id');
	}

	/* Relationship between the vehicle an photos 
	** A vehicle can have many photos - photos.vehicleId references vehicle.id */
	public function photo()
	{
		return $this->hasMany('App\Photo');
	}

	public function enquiry()
	{
		return $this->hasMany('App\Enquiry', 'userID', 'id');
	}

	public function order()
	{
		return $this->hasMany('App\Order', 'vehicleId', 'id');
	}
	
}