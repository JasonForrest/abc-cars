<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	 protected $fillable = [
    'vehicleId',
    'buyerId'
    ];
    public function orderVehicle()
	{
	return $this->hasOne('App\Vehicle', 'id', 'vehicleID');
	}

	public function orderUser()
	{
	return $this->hasOne('App\User', 'id', 'userID');
	}
}
